-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
awful.rules = require("awful.rules")
awful.autofocus = require("awful.autofocus")
require("lgi").PangoCairo.FontMap.get_default():set_resolution(120)

-- Widget and layout library
local wibox = require("wibox")

-- Theme handling library
local beautiful = require("beautiful")

-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")


-- Load Debian menu entries
require("debian.menu")
--local revelation = require("revelation")
local audio = require("audio")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Startup error",
                     text = awesome.startup_errors })
end

function Print(text)
  naughty.notify({ preset = naughty.config.presets.normal,
                         title = "Debug",
                         text = tostring(text) })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Runtime error",
                         text = err })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, and wallpapers
beautiful.init("/home/falco/.config/awesome/theme.lua")
for s = 1, screen.count() do
  gears.wallpaper.maximized(beautiful.wallpaper, s, true)
end

-- This is used later as the default terminal and editor to run.
terminal = "xfce4-terminal -e \"ssh-agent bash\""
editor = os.getenv("EDITOR") or "nano"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
local layouts =
{
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier
}
-- }}}

-- {{{ Tags
-- Define a tag table which hold all screen tags.
tags = {}
for s = 1, screen.count() do
    -- Each screen has its own tag table.
    tags[s] = awful.tag({ "tl", "tl", "m", "m", "f", "tl", "tb", "tb", "fl" }, s, {
      layouts[2],
      layouts[2],
      layouts[2],
      layouts[10],
      layouts[11],
      layouts[2],
      layouts[2],
      layouts[2],
      layouts[1]
      })
end
-- }}}

-- {{{ Menu
-- Create a laucher widget and a main menu
myawesomemenu = {
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", awesome.quit }
}

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
                                    { "Debian", debian.menu.Debian_menu.Debian },
                                    { "open terminal", terminal }
                                  }
                        })

-- }}}

-- {{{ Wibox
-- Create a textclock widget
mytextclock = awful.widget.textclock()

-- Create a systray
mysystray = wibox.widget.systray()

-- Create an ACPI widget
local batterywidget = wibox.widget.textbox()
batterywidget:set_text("| Battery | ")
batterywidgettimer = timer({ timeout = 5 })
local function updateBattery()
  fh = assert(io.popen("acpi | grep -Eo \"([a-zA-Z]+), ([0-9]+%)\"", "r"))
  batterywidget:set_text(" |" .. (fh:read("*l") or "") .. " | ")
  fh:close()
end
batterywidgettimer:connect_signal("timeout", updateBattery)
updateBattery()
batterywidgettimer:start()

-- Create a wibox for each screen and add it
mywibox = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, awful.tag.viewnext),
                    awful.button({ }, 5, awful.tag.viewprev)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() then
                                                      awful.tag.viewonly(c:tags()[1])
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({ width=250 })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))

for s = 1, screen.count() do
    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                           awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))
    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, mytaglist.buttons)

    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)

    -- Create the wibox
    mywibox[s] = awful.wibox({ position = "top", screen = s })

    -- Widgets that are aligned to the left
    local left_layout = wibox.layout.fixed.horizontal()
    --left_layout:add(mylauncher)
    left_layout:add(mytaglist[s])
    left_layout:add(mypromptbox[s])

    -- Widgets that are aligned to the right
    local right_layout = wibox.layout.fixed.horizontal()
    right_layout:add(batterywidget)
    if s == 1 then right_layout:add(wibox.widget.systray()) end
    right_layout:add(mytextclock)
    right_layout:add(mylayoutbox[s])

    -- Now bring it all together (with the tasklist in the middle)
    local layout = wibox.layout.align.horizontal()
    layout:set_left(left_layout)
    layout:set_middle(mytasklist[s])
    layout:set_right(right_layout)

    mywibox[s]:set_widget(layout)
    mywibox[s].visible = false
end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end)
))
-- }}}

-- {{{ Key bindings
local touchpadEnabled = 0
globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey, "Shift"   }, "Tab",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "w", function ()
      for s = 1, screen.count() do
        mywibox[s].visible = not mywibox[s].visible
      end
    end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.util.spawn(terminal) end),
    awful.key({ modkey, "Control" }, "r", awesome.restart),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit),

    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey,           }, "space", function () awful.layout.inc(layouts,  1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(layouts, -1) end),

    awful.key({ modkey, "Control" }, "n", awful.client.restore),

    -- Prompt
    awful.key({ modkey },            "r",     function () mypromptbox[mouse.screen]:run() end),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run({ prompt = "Run Lua code: " },
                  mypromptbox[mouse.screen].widget,
                  awful.util.eval, nil,
                  awful.util.getdir("cache") .. "/history_eval")
              end),


    awful.key({ modkey, "Control" }, "Tab",   revelation),
    -- Program binds
    awful.key({ modkey,           }, "l", function () awful.util.spawn("xscreensaver-command -lock") end),
    -- Thesis windows
    awful.key({ modkey,           }, "t", function ()
        awful.util.spawn(terminal .." --default-working-directory=/home/falco/Documents/thesis --title=thesisroot")
        awful.util.spawn(terminal .." --default-working-directory=/home/falco/Documents/thesis/test --title=thesistest")
        awful.util.spawn(terminal .." --default-working-directory=/home/falco/Documents/thesis/elm-compiler --title=elmcompiler")
    end),
    awful.key({ modkey            }, "e", function () awful.util.spawn("thunar") end),
    awful.key({ modkey            }, "a", function () awful.util.spawn("atom") end),
    awful.key({ modkey            }, "u", function () awful.util.spawn("pamac-updater") end),
    awful.key({ modkey            }, "c", function () awful.util.spawn("chromium") end),
    awful.key({ modkey            }, "z", function () awful.util.spawn("xfce4-appfinder") end),
    awful.key({ modkey, "Shift"   }, "c", function () awful.util.spawn("chromium") end),
    awful.key({ modkey, "Control" }, "s", function () awful.util.spawn("/home/falco/.local/share/Steam/steam.sh") end),
    awful.key({ modkey            }, "s", function () awful.util.spawn("subl3") end),
    awful.key({ modkey            }, "p", function () awful.util.spawn("vmpk") end),
    awful.key({ modkey            }, "v", function () awful.util.spawn("vinagre") end),
    awful.key({                   }, "XF86MonBrightnessUp", function () awful.util.spawn("xbacklight -inc 20") end),
    awful.key({                   }, "XF86MonBrightnessDown", function () awful.util.spawn("xbacklight -dec 20") end),
    awful.key({                   }, "XF86AudioLowerVolume", function () awful.util.spawn("pactl -- set-sink-volume 1 -10%") end),
    awful.key({                   }, "XF86AudioRaiseVolume", function () awful.util.spawn("pactl -- set-sink-volume 1 +10%") end),
    awful.key({ modkey            }, "BackSpace", function ()
                                        awful.util.spawn('xinput set-prop "SynPS/2 Synaptics TouchPad" "Device Enabled" '..touchpadEnabled)
                                        touchpadEnabled = (touchpadEnabled == 0) and 1 or 0
                                      end),
    -- Restart the system
    awful.key({ modkey, "Shift" }, "XF86PowerOff", function () awful.util.spawn("systemctl reboot") end),
    -- Shutdown
    awful.key({                   }, "XF86PowerOff", function () awful.util.spawn("systemctl poweroff") end),
    awful.key({                   }, "XF86Display", function () awful.util.spawn("systemctl hibernate") end),
    awful.key({                   }, "XF86AudioMute", function() audio.muteToggle() end),
    awful.key({ modkey            }, "XF86PowerOff", function() awful.util.spawn("systemctl suspend") end),
    awful.key({                   }, "XF86Search", function () awful.util.spawn("dbus-send --system --print-reply --dest=\"org.freedesktop.UPower\" /org/freedesktop/UPower org.freedesktop.UPower.Hibernate") end),
    awful.key({ modkey, "Control" }, "Up", function() awful.util.spawn("xrandr --output VGA1 --above LVDS1 --mode 1680x1050 --noprimary --output LVDS1 --mode 1366x768")end),
    awful.key({ modkey, "Control" }, "Down", function() awful.util.spawn("xrandr --output VGA1 --same-as LVDS1 --mode 1366x768") end),
    awful.key({ modkey, "Shift"   }, "Down", function() awful.util.spawn("xrandr --output VGA1 --same-as LVDS1 --mode 1024x768") end),
    awful.key({ modkey, "Shift"   }, "Down", function() awful.util.spawn("xrandr --output VGA1 --same-as LVDS1 --output LVDS1 --mode 1024x768") end),
    awful.key({ modkey, "Control" }, "q", function() awful.util.spawn("xkill") end),
    awful.key({ modkey, "Control" }, "s", function() awful.util.spawn("awsetbg -r /home/falco/Pictures/Wallpaper") end),
    awful.key({ modkey            }, "g", function() awful.util.spawn("kmag") end),
    awful.key({                   }, "Print", function() awful.util.spawn("zscreen") end)
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey,           }, "k",      function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey, "Shift"   }, "r",      function (c) c:redraw()                       end),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)
)

local focusWindowTimer = timer({ timeout = 0.03 })
focusWindowTimer:connect_signal("timeout",
  function()
    local c = awful.mouse.client_under_pointer()

    if c then
      client.focus = c;
      c:raise()
    end
    focusWindowTimer:stop()

    selectedTag = awful.tag.selected()
    if selectedTag then
      cls = selectedTag:clients()
      for k, v in pairs(cls) do
        if v.class == "hl2_linux" then
          v.minimized = false
        end
      end
    end
  end)

for _, scr in pairs(tags) do
  for _, tag in pairs(scr) do
    tag:connect_signal("property::selected", function(t)
      focusWindowTimer:start()
    end)
  end
end

client.connect_signal("new", function(c)
  local focusWindowTimer = timer({ timeout = 0.03 })
  focusWindowTimer:connect_signal("timeout",
    function()
      if type(c) == "client" and type(client) == "table" and c and client and client.focus then
      	-- client.focus = c
      end

      focusWindowTimer:stop()
    end)
  focusWindowTimer:start()
end)

-- Compute the maximum number of digit we need, limited to 9
keynumber = 0
for s = 1, screen.count() do
   keynumber = math.min(9, math.max(#tags[s], keynumber));
end

-- Bind F keys to later tags
for i = 6, keynumber do
  globalkeys = awful.util.table.join(globalkeys,
      awful.key({ modkey }, "F" .. i - 5,
                  function ()
                        local screen = mouse.screen
                        if tags[screen][i] then
                            awful.tag.viewonly(tags[screen][i])
                        end
                  end),
        awful.key({ modkey, "Control" }, "F" .. i - 5,
                  function ()
                      local screen = mouse.screen
                      if tags[screen][i] then
                          awful.tag.viewtoggle(tags[screen][i])
                      end
                  end),
        awful.key({ modkey, "Shift" }, "F" .. i - 5,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.movetotag(tags[client.focus.screen][i])
                      end
                  end),
        awful.key({ modkey, "Control", "Shift" }, "F" .. i - 5,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.toggletag(tags[client.focus.screen][i])
                      end
                  end)
    )
end

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, keynumber do
    globalkeys = awful.util.table.join(globalkeys,
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        if tags[screen][i] then
                            awful.tag.viewonly(tags[screen][i])
                        end
                  end),
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      if tags[screen][i] then
                          awful.tag.viewtoggle(tags[screen][i])
                      end
                  end),
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.movetotag(tags[client.focus.screen][i])
                      end
                  end),
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.toggletag(tags[client.focus.screen][i])
                      end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 2, function (c) client.focus = c; c:kill() end),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = true,
                     floating = false,
                     keys = clientkeys,
                     buttons = clientbuttons } },
    { rule = { class = "Debconf-communicate" },
      properties = { floating = true, tag = tags[1][9] } },
    { rule = { class = "MPlayer" },
      properties = { floating = true } },
    { rule = { class = "Sublime_text" },
      properties = { tag = tags[1][6] } },
    { rule = { class = "Subl3" },
      properties = { tag = tags[1][6] } },
    { rule = { class = "Kmag" },
      properties = { floating = true } },
    { rule = { class = "pinentry" },
      properties = { floating = true } },
    { rule = { class = "gimp" },
      properties = { floating = true,  } },
    { rule = { class = "Thunar" },
      properties = { floating = false, fullscreen = false } },
    { rule = { class = "Firefox"},
      properties = { tag = tags[1][3] } },
    -- Google chrome
    { rule = { class = "chromium" },
      properties = { tag = tags[1][3] } },
    { rule = { class = ".*chrome" },
      properties = { tag = tags[1][3] } },
    -- Google chrome plugins, hangouts
    --{ rule = { class = ".*chrome", role = "pop-up" },
     -- properties = { tag = tags[1][8] } },
    --{ rule = { class = "Google-chrome", name = "Hangouts" },
     -- properties = { tag = tags[1][8], floating = false } },
    { rule = { class = "Nautilus" },
      properties = { floating = false } },
    { rule = { class = "Plugin-container" },
      properties = { fullscreen = true, floating = true } },
    { rule = { name = "TeamViewer Panel" },
      properties = { fullscreen = false, floating = true } },
    { rule = { class = "Xfce4-terminal" },
      properties = { floating = false, size_hints_honor = false } },
    { rule = { name = "File Operations" },
      properties = { floating = true } },
    { rule = { class = "Steam" },
      properties = { tag = tags[1][7], floating = false }},
      except = { name = ".* - Chat" },
    { rule = { class = "Discord" },
      properties = { tag = tags[1][8], floating = false }},
    { rule = { class = "Thunderbird" },
      properties = { tag = tags[1][4] }},
    { rule = { name = "Friends" },
      properties = { tag = tags[1][8] }},
    { rule = { class = "hl2_linux" },
      properties = { tag = tags[1][5], floating = true, fullscreen = true }},
    { rule = { class = "Skype" },
      properties = { tag = tags[1][8] }},
    { rule = { class = "Pidgin" },
      properties = { tag = tags[1][8] }},
    { rule = { name = ".*Chat" },
      properties = { tag = tags[1][8] }},
    { rule = { class = "Codeblocks" },
      properties = { floating = false }},
    { rule = { class = "Pamac-updater" },
      properties = { floating = false }},
    { rule = { name = "Form" },
      properties = { floating = true }},
    { rule = { name = "Google+ Hangouts is sharing your screen with.*" },
      properties = { floating = true }},
    { rule = { name = "thesistest" },
      properties = { tag = tags[1][2] }},
    { rule = { name = "thesisroot" },
      properties = { tag = tags[1][1] }},
    { rule = { name = "elmcompiler" },
      properties = { tag = tags[1][1] }},
    { rule_any = { type = {"dialog", "splash"} },
      properties = { floating = true, fullscreen = false }},
    -- Set Firefox to always map on tags number 2 of screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { tag = tags[1][2] } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)
    -- Add a titlebar
    -- awful.titlebar.add(c, { modkey = modkey })

    -- Enable sloppy focus
    c:connect_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

function run_once(cmd)
  findme = cmd
  firstspace = cmd:find(" ")
  if firstspace then
    findme = cmd:sub(0, firstspace-1)
  end
  awful.util.spawn_with_shell("pgrep -u $USER -x " .. findme .. " > /dev/null || (" .. cmd .. ")")
end

-- run_once("xscreensaver")
-- run_once("feh --randomize --bg-fill /home/btsync/wallpaper/safe/")
-- run_once("nm-applet")
-- run_once("syndaemon -t -k -i 2 -d")
--run_once("xinput set-prop \"SynPS/2 Synaptics TouchPad\" \"Device Enabled\" 0")
-- run_once("synapse -s")
--run_once("btsync start")
-- run_once("dropboxd start")
-- run_once("skype")
--run_once("pidgin")
-- run_once("xscreensaver -no-splash")
-- run_once("setxkbmap -variant intl")
-- run_once("eval `ssh-agent -s`")
--run_once("shutter --min_at_startup")
-- run_once("/opt/extras.ubuntu.com/touchpad-indicator/bin/touchpad-indicator")

-- Policy kit (authentication) Always make sure this is running somehow.
-- Otherwise programs might not be able to request for superuser permissions
-- run_once("numlockx")
-- awful.util.spawn_with_shell("synclient TapButton3=3 TapButton2=2 CircularScrolling=1")
run_once("xfce4-clipman")
run_once("manjaro-settings-manager-daemon")
run_once("nm-applet")
run_once("/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1")
run_once("pa-applet")
run_once("light-locker")
run_once("pamac-tray")
run_once("/usr/bin/VBoxClient-all")
run_once("xfsettingsd")
run_once("zeitgeist-datahub")
