local awful = require('awful')

module("audio")
local isMuted = 0

function muteToggle()
	isMuted = (isMuted == 1) and 0 or 1
	awful.util.spawn("pactl set-sink-mute 0 " .. isMuted)
end